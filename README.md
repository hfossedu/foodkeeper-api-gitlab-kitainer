# FoodKeeper-API-GitLab Kitainer

This project creates Docker images that run GitLab with FoodKeeper-API
installed.

## Using a GitLab Kitainer

See [GitLab Kitainerize](https://gitlab.com/hfossedu/gitlab-kitainerize).

## Versions of Images Published to Docker Hub

### hfossedu/foodkeeper-api-gitlab:0.4.0

* Created 2020-02-26
* FoodKeeper-API
  * Date: 2020-02-26
  * Commit number: 736196ca237b239cdb2cb5b827352ee05e10c77d
* GitLab: gitlab/gitlab-ce:12.8.1-ce.0

### hfossedu/foodkeeper-api-gitlab:0.3.0

* Created 2020-02-??
* FoodKeeper-API
  * Date: 2020-02-??
  * Commit number: 32ae156cb1db8df3d01a24aa47a0ae9f8424fc36
* GitLab: gitlab/gitlab-ce:12.7.5-ce.0

## Create and Publishing a New Docker Image

These are high-level instructions for creating and publishing to Docker Hub a
new docker image.

1. Use GitLab to export the project.
2. Use <https://gitlab.com/hfossedu/kitainer/-/tree/master/gitlab-kitainerize> to build the kitainer.
3. Add a new entry to this file for the new image version.
4. Push the new image to Docker Hub.
5. Update the README.md on Docker Hub for the project.

---
Copyright 2020, Stoney Jackson

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
